import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Player implements KeyListener{

	private int X,Y;
	private int dy;
	private int dx;
	private boolean jump;
	
	//~GETTER'S
	public int getPlayerX()
	{
		return X;
	}
	
	public int getPlayerY()
	{
		return Y;
	}
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	public void setPlayerX(int s)
	{
		X=s;
	}
	
	public void setPlayerY(int s)
	{
		Y=s;
	}
	
	public Player()
	{
		X=50;
		Y=50;
	}
	
	public void paint(Graphics g)
	{
		g.setColor(Color.ORANGE);
		g.fillOval(X, Y, 10, 20);
	}

	
	public void update(MainClass mc)
	{	
		mc.addKeyListener(this);
		X += dx;
		Y +=4;	
		jump();
		MapBorder(mc);
	}

	@Override
	public void keyPressed(KeyEvent e) {
				switch(e.getKeyCode()){
						case KeyEvent.VK_RIGHT:{
							dx=3;
							break;
						
						}
						case KeyEvent.VK_LEFT:{
							dx=-3;
							break;
						}
						
						case KeyEvent.VK_UP:{
							//dy=-8;
							break;
						}
						
						case KeyEvent.VK_DOWN:{
							//dy=8;
							break;
						}
						case KeyEvent.VK_SPACE:{
							jump=true;
							break;
						}
				}		
		}

	@Override
	public void keyReleased(KeyEvent e) {
				switch(e.getKeyCode()){
						case KeyEvent.VK_RIGHT:{
							dx=0;
							break;
						
						}
						case KeyEvent.VK_LEFT:{
							dx=0;
							break;
						}
						
						case KeyEvent.VK_UP:{
							dy=0;
							break;
						}
						
						case KeyEvent.VK_DOWN:{
							dy=0;
							break;
						}
						case KeyEvent.VK_SPACE:{
							jump=false;
							
							break;
						}
				}
		}

	@Override
	public void keyTyped(KeyEvent e) {}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	public void MapBorder(MainClass mc)
	{
		if(X<=0){X=0;};
		if(X>=mc.getWidth()-20){X=mc.getWidth()-20;};
		
		if(Y<=0){Y=0;}
		if(Y>=mc.getHeight()-20){Y=mc.getHeight()-20;};
	}
	
	
	public void jump()
	{
		if(jump)
		{
				for(int i=0;i<10;i++)
				{				
					Y-=2;
					Y++;
				}
		}
	}
	
	
	
	
	
	
	
	
}
