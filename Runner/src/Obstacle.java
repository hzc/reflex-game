import java.awt.Graphics;
import java.awt.image.BufferedImage;

import app.ResourceLoader;


public class Obstacle {

	protected int X, Y,Speed;
	private BufferedImage wall;
	
	//~GETTER'S
		public int getobsX()
		{
			return X;
		}
		
		public int getobsY()
		{
			return Y;
		}
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	
	public Obstacle()
	{
		X = 650;
		Y = 0;
		Speed = 7;
		wall = ResourceLoader.getImage("/wall.jpg");
	}
	
	public void paint(Graphics g)
	{
		g.drawImage(wall, X, Y, null);
		//g.fillRect(X, Y, 60, 300);

	}
	
	public void update()
	{
		X -= Speed;
		
		if(X<=-100)
			X=650;
	}

	
}
