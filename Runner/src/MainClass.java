import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import app.ResourceLoader;

public class MainClass extends Applet implements Runnable {

private static final long serialVersionUID = 1L;
Thread thread = new Thread(this);

boolean running = true;
Player player;
Obstacle obstacle;
WhiteHole WH;
Collision col;

private Image buffer;
private BufferedImage background;
Graphics dbg;
int points=0;
int lvl;


	public void init()
	{
		setSize(600,300);
		player = new Player();
		player.MapBorder(this);
		col=new Collision();
		obstacle = new Obstacle();
	    WH = new WhiteHole();
		background = ResourceLoader.getImage("/background.jpg");
		
	}
public void start(){ thread.start(); }
public void stop(){running=false;}
public void delete(){running=false;}

public void run(){
	
	while(running)
	{
		repaint();
		
		player.update(this);
		obstacle.update();
		WH.update(obstacle.X);
		
		if(col.CollisionTest(player,obstacle,WH))
		{
				player.setPlayerX(10);
		}

		try {
			Thread.sleep(20);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
};

	public void update(Graphics g)
		{
			buffer = createImage(600,300);
			dbg = buffer.getGraphics();		
			paint(dbg);
			g.drawImage(buffer,0, 0, this);
		}
	
	public void paint(Graphics g)
		{	
			g.drawImage(background, 0, 0, null);
			obstacle.paint(g);
			WH.paint(g);
			player.paint(g);
			
		}

}
