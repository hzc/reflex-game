import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;


 	public class WhiteHole extends Obstacle {

	protected int x , y; //white Hole :D

		public int randomize()
	{
		Random rand = new Random();
		int R = rand.nextInt(250);
		return R;
	}
		
	public WhiteHole()
	{
		x = getobsX();
		y = randomize();
	}
	
	public void paint(Graphics g)
	{
		g.setColor(Color.WHITE);
		g.fillRect(x, y, 60, 60);
	}

	public void update(int X)
	{
		x=X;
		if(x<=-90)
		{
			x=650;
			y=randomize();
		}
		
	}
	
	
	
	
	
	
	
}
